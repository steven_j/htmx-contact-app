<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Contact $contact
 */
?>

<?php $this->start('tb_actions'); ?>
<li><?= $this->Html->link(
    __('Edit Contact'),
    ['action' => 'edit', $contact->id],
    ['class' => 'nav-link'],
) ?></li>
<li><?= $this->Form->postLink(
    __('Delete Contact'),
    ['action' => 'delete', $contact->id],
    [
        'confirm' => __('Are you sure you want to delete # {0}?', $contact->id),
        'class' => 'nav-link',
    ],
) ?></li>
<li><?= $this->Html->link(
    __('List Contacts'),
    ['action' => 'index'],
    ['class' => 'nav-link'],
) ?> </li>
<li><?= $this->Html->link(
    __('New Contact'),
    ['action' => 'add'],
    ['class' => 'nav-link'],
) ?> </li>
<?php $this->end(); ?>
<?php $this->assign(
    'tb_sidebar',
    '<ul class="nav flex-column">' . $this->fetch('tb_actions') . '</ul>',
); ?>

<div class="mb-3">
    <h3><?= h($contact->givenName) ?></h3>
    <div class="table-responsive">
        <table class="table table-striped">
            <tr>
                <th scope="row"><?= __('GivenName') ?></th>
                <td><?= h($contact->givenName) ?></td>
            </tr>
            <tr>
                <th scope="row"><?= __('FamilyName') ?></th>
                <td><?= h($contact->familyName) ?></td>
            </tr>
            <tr>
                <th scope="row"><?= __('Phone') ?></th>
                <td><?= h($contact->phone) ?></td>
            </tr>
            <tr>
                <th scope="row"><?= __('Email') ?></th>
                <td><?= h($contact->email) ?></td>
            </tr>
            <tr>
                <th scope="row"><?= __('Id') ?></th>
                <td><?= $this->Number->format($contact->id) ?></td>
            </tr>
        </table>
    </div>
</div>

<div class="d-flex gap-3">
    <?= $this->Html->link(
        __('Edit'),
        ['action' => 'edit', $contact->id],
        ['class' => 'btn btn-primary'],
    ) ?>
    <?= $this->Html->link(
        __('Back'),
        ['action' => 'index'],
        ['class' => 'btn btn-secondary'],
    ) ?>
</div>
