<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Contact $contact
 */
?>

<?php $this->start('tb_actions'); ?>
<li><?= $this->Html->link(
    __('List Contacts'),
    ['action' => 'index'],
    ['class' => 'nav-link'],
) ?></li>
<?php $this->end(); ?>
<?php $this->assign(
    'tb_sidebar',
    '<ul class="nav flex-column">' . $this->fetch('tb_actions') . '</ul>',
); ?>

<div class="mb-3">
    <?= $this->Form->create($contact, ['novalidate' => true]) ?>
    <fieldset>
        <legend><?= __('Add Contact') ?></legend>
        <?php
        echo $this->Form->control('givenName');
        echo $this->Form->control('familyName');
        echo $this->Form->control('phone');
        echo $this->Form->control('email');
        ?>
    </fieldset>
    <?= $this->Form->button(__('Save'), ['class' => 'w-100 primary']) ?>
    <?= $this->Form->end() ?>
</div>

<div>
<?= $this->Html->link(
    __('Back'),
    ['action' => 'index'],
    ['class' => 'btn btn-secondary'],
) ?>
</div>
