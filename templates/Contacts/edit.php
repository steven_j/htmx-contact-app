<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Contact $contact
 */
?>
<?= $this->Html->meta('htmx-config', '{"methodsThatUseUrlParams": ["get"]}') ?>

<?php $this->start('tb_actions'); ?>
<li><?= $this->Form->postLink(
    __('Delete'),
    ['action' => 'delete', $contact->id],
    [
        'confirm' => __('Are you sure you want to delete # {0}?', $contact->id),
        'class' => 'nav-link',
    ],
) ?></li>
<li><?= $this->Html->link(
    __('List Contacts'),
    ['action' => 'index'],
    ['class' => 'nav-link'],
) ?></li>
<?php $this->end(); ?>
<?php $this->assign(
    'tb_sidebar',
    '<ul class="nav flex-column">' . $this->fetch('tb_actions') . '</ul>',
); ?>

<div class="mb-3">
    <?= $this->Form->create($contact, ['novalidate' => true]) ?>
    <fieldset>
        <legend><?= __('Edit Contact') ?></legend>
        <?php
        echo $this->Form->control('givenName');
        echo $this->Form->control('familyName');
        echo $this->Form->control('phone');
        echo $this->Form->control('email');
        ?>
    </fieldset>
    <div class="d-flex gap-3">
        <?= $this->Form->button(__('Save'), ['class' => 'w-100 primary']) ?>
    </div>
    <?= $this->Form->end() ?>
</div>
<div class="d-flex gap-3">
    <?= $this->Form->create($contact, [
        'url' => ['action' => 'delete', $contact->id],
        'type' => 'delete',
    ]) ?>
    <?= $this->Form->submit('Delete', [
        'class' => 'danger',
        'hx-delete' => $this->Html->Url->build([
            'action' => 'delete',
            $contact->id,
        ]),
        'hx-confirm' =>
            'Are you sure you want to delete the contact ' .
            $contact->givenName .
            ' ' .
            $contact->familyName,
        'hx-target' => 'body',
        'hx-push-url' => 'true',
    ]) ?>
    <?= $this->Form->end() ?>
    <?= $this->Html->link(
        __('Back'),
        ['action' => 'index'],
        ['class' => 'btn btn-secondary'],
    ) ?>
</div>
