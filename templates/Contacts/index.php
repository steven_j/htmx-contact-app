<?php
/**
 * @var \App\View\AppView $this
 * @var iterable<\App\Model\Entity\Contact> $contacts
 * @var mixed $q
 */
?>
<div>
    <div class="d-flex justify-content-between">
        <div>
            <h1>Contacts</h1>
            <p>A list of all your contacts.</p>
        </div>
        <div class="mt-4 sm:ml-16 sm:mt-0 sm:flex-none">
            <?= $this->Html->link(
                'Add contact',
                ['action' => 'add'],
                ['class' => 'button-primary'],
            ) ?>
        </div>
    </div>
    <div class="d-flex justify-content-between">
        <div>
            <?= $this->Form->create(null, [
                'type' => 'get',
                'class' => 'd-flex gap-3',
            ]) ?>
                <?= $this->form->input('q', [
                    'type' => 'search',
                    'label' => 'Search',
                    'id' => 'search',
                    'value' => $q,
                    'hx-get' => $this->Url->build(['action' => 'index']),
                    'hx-trigger' => 'search, keyup delay:200ms changed',
                    'hx-target' => 'tbody',
                    'hx-swap' => 'outerhtml',
                    'hx-push-url' => 'true',
                    'hx-indicator' => '#spinner',
                ]) ?>
                <?= $this->Form->submit('Search') ?>
            <?= $this->Form->end() ?>
            <div id="spinner" class="spinner-border htmx-indicator" role="status">
              <span class="visually-hidden">Loading...</span>
            </div>
        </div>
        <div>
            <div id="archive-ui" hx-target="this" hx-swap="outerHTML" class="h-8 w-80">
                <button hx-post="@{/contacts/archives}" class="button-secondary"> Download archive </button>
            </div>
        </div>
    </div>
    <div>
        <div>
            <div>
                <table class="table">
                    <thead>
                    <tr>
                        <th scope="col">Name</th>
                        <th scope="col">Phone</th>
                        <th scope="col">Email</th>
                        <th scope="col">
                            <span class="visually-hidden">Edit or View</span>
                        </th>
                    </tr>
                    </thead>
                    <tbody>
                        <?= $this->element('Contacts/list') ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
