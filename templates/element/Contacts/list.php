<?php
/**
 * @var \App\View\AppView $this
 * @var mixed $q
 * @var \App\Model\Entity\Contact[]|\Cake\Collection\CollectionInterface $contacts
 */
?>
<?php foreach ($contacts as $contact) : ?>
<tr>
    <th scope="row">
        #<?= $contact->id ?>
        <?= $contact->givenName ?>
        <?= $contact->familyName ?>
    </th>
    <td><?= $contact->phone ?></td>
    <td><?= $contact->email ?></td>
    <td>
        <?= $this->Html->link('Edit', [
            'action' => 'edit',
            $contact->id,
        ]) ?>
        <?= $this->Html->link('View', [
            'action' => 'view',
            $contact->id,
        ]) ?>
    </td>
</tr>
<?php endforeach; ?>
<?php if ($this->Paginator->hasNext()) : ?>
<tr>
    <td colspan="4">
        <div>
            <span
                hx-get="<?= $this->Url->build([
                    'action' => 'index',
                    '?' => [
                        'page' => $this->Paginator->current() + 1,
                        'q' => $q,
                    ],
                ]) ?>"
                hx-trigger="revealed"
                hx-select="tr"
                hx-target="closest tr"
                hx-swap="outerHTML"
            >
            </span>
        </div>
    </td>
</tr>
<?php endif; ?>
