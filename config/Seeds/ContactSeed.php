<?php
declare(strict_types=1);

use Migrations\AbstractSeed;

/**
 * Contact seed.
 */
class ContactSeed extends AbstractSeed
{
    /**
     * Run Method.
     *
     * Write your database seeder using this method.
     *
     * More information on writing seeds is available here:
     * https://book.cakephp.org/phinx/0/en/seeding.html
     *
     * @return void
     */
    public function run(): void
    {
        $data = [
            [
                'givenName' => 'Wim',
                'familyName' => 'Deblauwe',
                'phone' => '555-789-999',
                'email' => 'wim@example.com',
            ],
            [
                'givenName' => 'John',
                'familyName' => 'Doe',
                'phone' => '555-123-456',
                'email' => 'john@example.com',
            ],
            [
                'givenName' => 'Ada',
                'familyName' => 'Lovelace',
                'phone' => '555-873-321',
                'email' => 'ada@lovelace.com',
            ],
        ];

        $table = $this->table('contacts');
        $table->insert($data)->save();
    }
}
