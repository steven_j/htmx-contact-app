# HTMX Contact App

[Contact App](https://hypermedia.systems/building-a-contacts-app-with-hyperview/) implementation with CakePHP.

With additional reading throught [Modern frontends with htmx](https://leanpub.com/modern-frontends-with-htmx).

-   [modern-frontends-with-htmx-sources](https://github.com/wimdeblauwe/modern-frontends-with-htmx-sources)
