default: help

# Perl Colors, with fallback if tput command not available
GREEN  := $(shell command -v tput >/dev/null 2>&1 && tput -Txterm setaf 2 || echo "")
BLUE   := $(shell command -v tput >/dev/null 2>&1 && tput -Txterm setaf 4 || echo "")
WHITE  := $(shell command -v tput >/dev/null 2>&1 && tput -Txterm setaf 7 || echo "")
YELLOW := $(shell command -v tput >/dev/null 2>&1 && tput -Txterm setaf 3 || echo "")
RESET  := $(shell command -v tput >/dev/null 2>&1 && tput -Txterm sgr0 || echo "")

# Add help text after each target name starting with '\#\#'
# A category can be added with @category
HELP_FUN = \
    %help; \
    while(<>) { push @{$$help{$$2 // 'options'}}, [$$1, $$3] if /^([a-zA-Z\-]+)\s*:.*\#\#(?:@([a-zA-Z\-]+))?\s(.*)$$/ }; \
    print "usage: make [target]\n\n"; \
    for (sort keys %help) { \
    print "${WHITE}$$_:${RESET}\n"; \
    for (@{$$help{$$_}}) { \
    $$sep = " " x (32 - length $$_->[0]); \
    print "  ${YELLOW}$$_->[0]${RESET}$$sep${GREEN}$$_->[1]${RESET}\n"; \
    }; \
    print "\n"; }

help:
	@perl -e '$(HELP_FUN)' $(MAKEFILE_LIST)

SHELL=/bin/bash
DOCKER_COMPOSE = docker compose -f ./infra/docker-compose.yml
npm=npm --prefix ./infra -s run

########################################
#                 APP                  #
########################################
.PHONY: config
config: ##@App generate conf
	@cp ./config/app_local.dev.php ./config/app_local.php
	@sed -i "s/__SALT__/$(shell openssl rand -base64 32 | tr -d /=+)/" ./config/app_local.php

show-logs: ##@App show logs
	tail -f logs/*.log

########################################
#                 QA                   #
########################################
cs-check: ##@QA
	composer run-script cs-check

cs-fix: ##@QA
	composer run-script cs-fix
	npx prettier --write .

static-check: ##@QA
	composer run-script stan

########################################
#            DEPENDENCIES              #
########################################
install: ##@Dependencies install dependencies
	@composer install

build: ##@Dependencies build
	@if [ -d ./vendor ]; then rm -rf ./vendor; fi
	composer install --ignore-platform-reqs --no-ansi --no-dev --no-interaction --no-plugins --no-progress --no-scripts --optimize-autoloader
